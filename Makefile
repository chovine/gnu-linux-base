.PHONY: all system-tools

binutils=/usr/bin/ld
linux-headers=/usr/include/linux
glibc=/usr/bin/ldd
zlib=/usr/lib/libz.so
gmp=/usr/lib/libgmp.so
mpfr=/usr/lib/libmpfr.so
mpc=/usr/lib/libmpc.so
gcc=/usr/bin/gcc
libstdcpp=/usr/include/c++/
rename_tools=/tools.old
diffutils=/usr/bin/diff 
coreutils=/usr/bin/dd
bash=/usr/bin/bash
make=/usr/bin/make
bison=/usr/bin/bison
gawk=/usr/bin/gawk
grep=/usr/bin/egrep
file=/usr/bin/file
m4=/usr/bin/m4
patch=/usr/bin/patch
sed=/usr/bin/sed
texinfo=/usr/bin/info
perl=/usr/bin/perl
tar=/usr/bin/tar
ncurses=/usr/lib/libncursesw.a
curl=/usr/bin/curl
gzip=/usr/bin/gzip
openssl=/usr/lib/libssl.a
shadow=/usr/sbin/useradd
kmod=/usr/bin/kmod
xz=/usr/bin/xz
ed=/usr/bin/ed
python=/usr/bin/python3
flex=/usr/bin/flex
bc=/usr/bin/bc
pv=/usr/bin/pv
find=/usr/bin/find
grub=/sbin/grub-install
pkg-config=/usr/bin/pkg-config
util-linux=/usr/bin/mount
systemd=/usr/bin/systemctl
ninja=/usr/bin/ninja
meson=/usr/bin/meson
autoconf=/usr/bin/autoconf
autogen=/usr/bin/autogen
guile=/usr/bin/guile
libtool=/usr/bin/libtool
libunistring=/usr/include/unistring
libffi=/usr/lib/libffi.so
libcap=/usr/lib/libcap.so
gc=/usr/lib/libgc.so
rsync=/usr/bin/rsync
gperf=/usr/bin/gperf
dbus=/usr/bin/dbus-daemon
expat=/usr/bin/xmlwf
procps=/usr/bin/ps
e2fsprogs=/sbin/mkfs.ext4
linux=/boot/vmlinuz
elfutils=/usr/lib/libelf.so.1
bzip2=/usr/bin/bunzip2
cpio=/usr/bin/cpio
efibootmgr=/usr/bin/efibootmgr
efivar=/usr/lib/libefivar.so
popt=/usr/include/popt.h
newt-pm=/usr/bin/newt

#export CFLAGS=-I/usr/include
#export CXXFLAGS=-I/usr/include

all: $(rename_tools)
tools: $(rename_tools)

$(autoconf): $(perl)
	MAKEFLAGS= ./mkpkg.sh autoconf.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i autoconf-*.tar.gz

$(autogen): $(autoconf) $(guile)
	MAKEFLAGS= ./mkpkg.sh autogen.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i autogen-*.tar.gz

$(libunistring):
	MAKEFLAGS= ./mkpkg.sh libunistring.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i libunistring-*.tar.gz

$(libtool): $(gcc) $(libunistring)
	MAKEFLAGS= ./mkpkg.sh libtool.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i libtool-*.tar.gz

$(gc):
	MAKEFLAGS= ./mkpkg.sh gc.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gc-*.tar.gz

$(rsync):
	MAKEFLAGS= ./mkpkg.sh rsync.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i rsync-*.tar.gz

$(libffi):
	MAKEFLAGS= ./mkpkg.sh libffi.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i libffi-*.tar.gz

$(guile):  $(gcc) $(libtool) $(libffi) $(gc)
	MAKEFLAGS= ./mkpkg.sh guile.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i guile-*.tar.gz

$(binutils):  $(zlib)
	MAKEFLAGS= ./mkpkg.sh binutils.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i binutils-*.tar.gz

$(linux-headers): $(binutils)
	MAKEFLAGS= ./mkpkg.sh linux_headers.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i linux-headers-*.tar.gz

$(glibc): $(linux-headers)
	MAKEFLAGS= ./mkpkg.sh glibc.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i glibc-*.tar.gz

$(libstdcpp): | $(glibc)
	MAKEFLAGS= ./mkpkg.sh libstdcpp.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i libstdcpp-*.tar.gz

$(zlib):
	MAKEFLAGS= ./mkpkg.sh zlib.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i zlib-*.tar.gz

$(gmp): | $(libstdcpp)
	MAKEFLAGS= ./mkpkg.sh gmp.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gmp-*.tar.gz

$(mpfr): | $(libstdcpp)
	MAKEFLAGS= ./mkpkg.sh mpfr.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i mpfr-*.tar.gz

$(mpc): | $(libstdcpp)
	MAKEFLAGS= ./mkpkg.sh mpc.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i mpc-*.tar.gz

$(gcc): $(gmp) $(mpfr) $(mpc) $(zlib)
	MAKEFLAGS= ./mkpkg.sh gcc.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gcc-*.tar.gz

$(coreutils): $(gcc)
	MAKEFLAGS= ./mkpkg.sh coreutils.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i coreutils-*.tar.gz

$(diffutils): $(gcc)
	MAKEFLAGS= ./mkpkg.sh diffutils.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i diffutils-*.tar.gz

$(ncurses): $(gcc)
	MAKEFLAGS= ./mkpkg.sh ncurses.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i ncurses-*.tar.gz

$(file): $(gcc)
	MAKEFLAGS= ./mkpkg.sh file.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i file-*.tar.gz

$(gawk): $(file)
	MAKEFLAGS= ./mkpkg.sh gawk.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gawk-*.tar.gz

$(grep): $(gcc)
	MAKEFLAGS= ./mkpkg.sh grep.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i grep-*.tar.gz

$(bison): $(m4)
	MAKEFLAGS= ./mkpkg.sh bison.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i bison-*.tar.gz

$(pkg-config): $(gcc)
	MAKEFLAGS= ./mkpkg.sh pkg-config.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i pkg-config-*.tar.gz

$(bash): $(gcc) $(ncurses)
	MAKEFLAGS= ./mkpkg.sh bash.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i bash-*.tar.gz

$(make): $(gcc)
	MAKEFLAGS= ./mkpkg.sh make.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i make-*.tar.gz

$(m4): $(gcc) $(patch)
	MAKEFLAGS= ./mkpkg.sh m4.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i m4-*.tar.gz

$(patch): $(gcc)
	MAKEFLAGS= ./mkpkg.sh patch.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i patch-*.tar.gz

$(sed): $(gcc)
	MAKEFLAGS= ./mkpkg.sh sed.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i sed-*.tar.gz

$(perl): $(gcc)
	MAKEFLAGS= ./mkpkg.sh perl.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i perl-*.tar.gz

$(tar): $(gcc)
	MAKEFLAGS= ./mkpkg.sh tar.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i tar-*.tar.gz

$(openssl): $(gcc) $(perl)
	MAKEFLAGS= ./mkpkg.sh openssl.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i openssl-*.tar.gz

$(gzip): $(gcc)
	MAKEFLAGS= ./mkpkg.sh gzip.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gzip-*.tar.gz

$(xz): $(gcc)
	MAKEFLAGS= ./mkpkg.sh xz.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i xz-*.tar.gz

$(flex): $(gcc)
	MAKEFLAGS= ./mkpkg.sh flex.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i flex-*.tar.gz

$(bc): $(gawk) $(ed)
	MAKEFLAGS= ./mkpkg.sh bc.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i bc-*.tar.gz

$(ed): $(gcc)
	MAKEFLAGS= ./mkpkg.sh ed.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i ed-*.tar.gz

$(pv): $(gcc)
	MAKEFLAGS= ./mkpkg.sh pv.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i pv-*.tar.gz

$(find): $(gcc)
	MAKEFLAGS= ./mkpkg.sh findutils.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i findutils-*.tar.gz

$(curl): $(openssl)
	MAKEFLAGS= ./mkpkg.sh curl.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i curl-*.tar.gz

$(texinfo): $(gcc) $(perl)
	MAKEFLAGS= ./mkpkg.sh texinfo.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i texinfo-*.tar.gz

$(python): $(gcc) $(libffi)
	MAKEFLAGS= ./mkpkg.sh python.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i python-*.tar.gz

$(rename_tools): | $(bash) $(coreutils) $(diffutils) $(make) $(sed) $(m4) $(texinfo) $(tar) $(curl) $(gzip) $(xz) $(bison) $(grep) $(flex) $(bc) $(pv) $(find) $(pkg-config) $(rsync) $(python) #$(autoconf) $(autogen)
	mv /tools /tools.old || echo

$(shadow): | $(rename_tools)
	MAKEFLAGS= ./mkpkg.sh shadow.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i shadow-*.tar.gz

$(kmod): | $(rename_tools)
	MAKEFLAGS= ./mkpkg.sh kmod.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i kmod-*.tar.gz

$(ninja): $(python)
	MAKEFLAGS= ./mkpkg.sh -v ninja.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i ninja-*.tar.gz

$(meson): $(ninja) $(libffi)
	MAKEFLAGS= ./mkpkg.sh meson.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i meson-*.tar.gz

$(gperf):
	MAKEFLAGS= ./mkpkg.sh gperf.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i gperf-*.tar.gz

$(bzip2):
	MAKEFLAGS= ./mkpkg.sh bzip2.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i bzip2-*.tar.gz

$(elfutils): $(bzip2)
	MAKEFLAGS= ./mkpkg.sh elfutils.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i elfutils-*.tar.gz

$(linux): $(elfutils) $(cpio)
	MAKEFLAGS= ./mkpkg.sh -n linux-zen.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i linux-zen-kernel-*.tar.gz

$(cpio):
	MAKEFLAGS= ./mkpkg.sh cpio.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i cpio-*.tar.gz

$(libcap):
	MAKEFLAGS= ./mkpkg.sh libcap.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i libcap-*.tar.gz

$(e2fsprogs):
	MAKEFLAGS= ./mkpkg.sh e2fsprogs.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i e2fsprogs-*.tar.gz

$(expat):
	MAKEFLAGS= ./mkpkg.sh expat.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i expat-*.tar.gz

$(efibootmgr): $(efivar) $(popt)
	MAKEFLAGS= ./mkpkg.sh efibootmgr.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i efibootmgr-*.tar.gz

$(efivar):
	MAKEFLAGS= ./mkpkg.sh efivar.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i efivar-*.tar.gz

$(popt):
	MAKEFLAGS= ./mkpkg.sh popt.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i popt-*.tar.gz

$(newt-pm): $(glibc)
	MAKEFLAGS= ./mkpkg.sh newt-pm.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i newt-pm-*.tar.gz

$(dbus): $(expat)
	MAKEFLAGS= ./mkpkg.sh dbus.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i dbus-*.tar.gz

$(systemd): $(meson) $(gperf) $(libcap) $(util-linux)
	MAKEFLAGS= ./mkpkg.sh systemd.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i systemd-*.tar.gz
	rm /etc/resolv.conf
	echo nameserver 8.8.8.8 > /etc/resolv.conf

$(procps): $(systemd)
	MAKEFLAGS= ./mkpkg.sh procps-ng.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i procps-ng-*.tar.gz

$(grub): $(efibootmgr) | $(rename_tools) $(python)
	MAKEFLAGS= ./mkpkg.sh grub.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i grub-*.tar.gz

$(util-linux): | $(rename_tools)
	MAKEFLAGS= ./mkpkg.sh util-linux.pkgspec &&\
	MAKEFLAGS= ./mkpkg.sh -i util-linux-*.tar.gz


system-tools: $(grub) $(shadow) $(kmod) $(grub) $(util-linux) $(systemd) $(dbus) $(procps) $(e2fsprogs) $(linux) $(newt-pm)