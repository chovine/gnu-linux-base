FAILED=""
for d in pkgspecs/*.pkgspec; do
    ./mkpkg.sh -n $d || FAILED="${FAILED} ${d}"
    echo "========================"
    echo $FAILED
    echo "========================"
done

echo $FAILED
mkdir pkgs
cp *.tar.gz pkgs
tar -acf pkgs.tar.gz pkgs 
