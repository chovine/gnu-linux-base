#!/bin/bash
set -e

cp pkgspecs/* .
rm -r /cross-tools ||
make && make -B && make -B && make system-tools && source makeall.sh