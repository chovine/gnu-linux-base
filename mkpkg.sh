#!/bin/bash
set -e
umask 022

function pretty() {
	bold=$(tput bold)
	normal=$(tput sgr0)
	echo "${bold}==> $1${normal}"
}

ORIG_DIR=$(pwd)
MKPKG_MAKE_FLAGS="-j$(nproc)"
TEST_PKG=0
NO_CLEAN=0
INSTALL=0
STRIP=1
TARGET_ROOT="/"

function clean() {
	if [ "$NO_CLEAN" -eq 0 ]; then
		pretty "Clean"
		echo $2
		rm -rf $TMPDIR $FAKEROOT
	fi
	exit $1
}

function stripall() {
	find $FAKEROOT/usr/lib -type f -name \*.a \
	-exec strip --strip-debug {} ';'

	find $FAKEROOT/lib $FAKEROOT/usr/lib -type f -name \*.so* ! -name \*dbg \
	-exec strip --strip-unneeded {} ';'

	find $FAKEROOT/{bin,sbin} $FAKEROOT/usr/{bin,sbin,libexec} -type f \
		-exec strip --strip-all {} ';'
}

# Default functions
function testsrc() {
	echo "No test to execute"
}

function postinstall() {
	echo "No postinstall to execute"
}

function move_libs() {
	cd $FAKEROOT
	if [ -d "usr/lib64" ]; then
		mkdir -p usr/lib
		mv usr/lib64/* usr/lib
		rm -r usr/lib64
		echo "Moved /usr/lib64 to /usr/lib"
	fi

	if [ -d "lib64" ]; then
		mkdir -p lib
		mv lib64/* lib
		rm -r lib64
		echo "Moved lib64 to lib"
	fi
}

TMPDIR=$(mktemp -d)

while getopts "vsticnr:" opt; do
  case ${opt} in
	r )
		TARGET_ROOT=$OPTARG
	;;
 	n ) 
		TMPDIR=$(uuidgen)
		mkdir $TMPDIR
		TMPDIR=`realpath $TMPDIR`
	;;
	s ) 
		STRIP=0
	;;
  	c ) 
		NO_CLEAN=1
	;;
	t ) 
		TEST_PKG=1
	;;
	i ) 
		INSTALL=1
	;;
    v ) 
	     set -x
	;;
    \? ) echo "Usage: mkpkg [-vti] PKGSPEC"
		exit
      ;;
  esac
done
shift $((OPTIND -1))

PKGSPEC=`realpath $1`
ARCHIVE=`realpath $1`

# Install
if [ "$INSTALL" -eq 1 ]; then
	pretty "Install"
	cd $TARGET_ROOT
	pv $ARCHIVE | tar --keep-directory-symlink --no-overwrite-dir -zx || clean "Install failed."
	PKGSPEC=`realpath tmp/$(basename $ARCHIVE .tar.gz).pkgspec`
	source $PKGSPEC
	if [ "$TARGET_ROOT" == "/" ]; then
		pretty "Post Install"
		postinstall || clean "Post install config failed."
	fi
	chmod 755 $TARGET_ROOT
	clean 0
fi

# Source PKGINFO
source $PKGSPEC


# Check validity
if [ -z "$PKG_NAME" ]; then
	echo "Invalid PKGSPEC."
	exit 1
fi

if [ -z "$PKG_VERSION" ]; then
	echo "Invalid PKGSPEC."
	exit 1
fi

cd $TMPDIR

# Get sources
pretty "Get sources"
for S in $PKG_SOURCE; do
curl -L --progress-bar --retry 10 --connect-timeout 10 --retry-all-errors --remote-name $S
done

# Prepare
pretty "Prepare sources"
prepare  || clean 1 "Prepare failed."
cd $TMPDIR

# Build
pretty "Build sources"
build  || clean 1 "Build failed."
cd $TMPDIR

# Test
if [ "$TEST_PKG" -eq 1 ]; then
	pretty "Test sources"
	testsrc || clean 1 "Test failed."
	cd $TMPDIR
fi

# Fakeroot install
pretty "Fakeroot install"
FAKEROOT=$(mktemp -d)
cd $FAKEROOT
if [ "$PKG_NAME" != "filesystem" ]; then
	mkdir -p tmp bin usr/{bin,lib,slib,libexec,sbin} lib sbin
fi
cd $TMPDIR
installsrc  || clean 1 "Fakeroot install failed."

#chmod 777 $FAKEROOT/tmp
#chmod -R 750 $FAKEROOT/{usr,bin,lib} 
if [ "$PKG_NAME" != "filesystem" ]; then
	if [ "$STRIP" -eq 1 ]; then
		stripall  || clean 1 "Stripping failed."
	fi
	move_libs
fi


pretty "Package"
cd $FAKEROOT
cp $PKGSPEC tmp/$PKG_NAME-$PKG_VERSION.pkgspec
ARCHIVE_NAME=$PKG_NAME-$PKG_VERSION.tar.gz
tar -acf /tmp/$ARCHIVE_NAME .
mv /tmp/$ARCHIVE_NAME $ORIG_DIR

clean 0

pretty "Done"
