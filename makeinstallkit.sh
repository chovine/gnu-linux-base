#!/bin/bash
set -xe

rm -rf install_kit || echo
mkdir -p install_kit
mkpkg -i -r install_kit install-scripts-1.tar.gz
mkpkg -i -r install_kit newt-pm-1.tar.gz
cp  *.tar.gz  install_kit/var/newt/pkgs
sed -i "s/pv/cat/g" install_kit/usr/bin/mkpkg 

cat > install_kit/env.sh << EOF
export PATH=\$PATH:\$(realpath usr/bin)
export PKGDIR=\$(realpath var/newt/pkgs)
EOF

cat > install_kit/sampleinstall.sh << EOS
set -xe
DISK=\$1
parted \$DISK mklabel gpt
parted \$DISK mkpart primary 0% 500M
parted \$DISK mkpart primary 500M 100%
mkfs.vfat \${DISK}p1
mkfs.ext4 \${DISK}p2
mount \${DISK}p2 /mnt
mkdir /mnt/boot
mount \${DISK}p1 /mnt/boot

mkdir /mnt/root

cat > /mnt/root/install.sh << EOF
newt install systemd
newt install grub
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
cp /etc/default/samplegrub.cfg /boot/grub/grub.cfg
passwd root
EOF

newt -r /mnt --pkgdir \$PKGDIR install base
cp \$PKGDIR/*  /mnt/var/newt/pkgs

EOS

cat > install_kit/basedir.sh << EOS
#VFS

FAKEROOT=\$1
mkdir -p \$FAKEROOT/{dev,sys,run,proc}

# Essential dirs
mkdir -p  \$FAKEROOT/{boot,home,mnt,opt,srv,local,var/newt,root,tmp,etc,usr/{bin,lib,sbin}}

echo 127.0.0.1 localhost.localdomain localhost > \$FAKEROOT/etc/hosts
echo nameserver 8.8.8.8 > \$FAKEROOT/etc/resolv.conf
echo "bootstrapped-system" > \$FAKEROOT/etc/hostname
cat > \$FAKEROOT/etc/passwd << EOF
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:/bin:/bin/false
daemon:x:2:6:/sbin:/bin/false
messagebus:x:27:27:D-Bus Message Daemon User:/dev/null:/bin/false
nobody:x:65534:65533:Unprivileged User:/dev/null:/bin/false
EOF

cat > \$FAKEROOT/etc/group << EOF
root:x:0:
bin:x:1:
sys:x:2:
kmem:x:3:
tty:x:5:
tape:x:4:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:27:
systemd-journal:x:28:
mail:x:30:
wheel:x:39:
nogroup:x:65533:
EOF

ln -s usr/bin \$FAKEROOT/bin
ln -s usr/sbin \$FAKEROOT/sbin
ln -s usr/lib \$FAKEROOT/lib
ln -s usr/lib \$FAKEROOT/usr/lib64
ln -s lib \$FAKEROOT/lib64
ln -s bin/bash \$FAKEROOT/usr/bin/sh
ln -s usr/bin/gcc \$FAKEROOT/usr/bin/cc

echo "Do not forget to copy the pkgs to /mnt and reinstall base + grub from the chroot ;-) "


cat > \$FAKEROOT/root/install.sh << EOF
newt install systemd
newt install grub
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
cp /etc/default/samplegrub.cfg /boot/grub/grub.cfg
EOF

EOS

tar -cf install_kit.tar install_kit
